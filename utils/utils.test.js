/* global it, describe */
const expect = require('expect')

const utils = require('./utils')

describe('Utils', () => {
  describe('#syncTests', () => {
    it('should add 2 numbers', () => {
      let res = utils.add(33, 11)
      expect(res).toBe(44).toBeA('number')
      // if (res !== 44) {
      //   throw new Error(`Expected 44, but got ${res}`)
      // }
    })

    // it('should expect some values', () => {
      // expect(21).toNotBe(11)
      // expect({name: 'Zoli'}).toNotEqual({name: 'Zoli'})
      // expect([1, 3, 5]).toExclude(2)
      // expect({
      //   name: 'Zoli',
      //   age: 33,
      //   location: 'Besnyo'
      // }).toInclude({
      //   age: 33
      // })
    // })

    it('should verify first- and last names are set', () => {
      let user = {
        age: 33,
        location: 'Budapest'
      }
      let res = utils.setName(user, 'Zoltan Virag')
      expect(res).toBeA('object').toInclude({
        firstName: 'Zoltan',
        lastName: 'Virag'
      })
    })

    it('should square the number', () => {
      let res = utils.square(11)
      expect(res).toBe(121).toBeA('number')
    })
  })

  describe('#AsyncTests', () => {
    // Asynchronous testing
    it('should async add 2 numbers', (done) => {
      utils.asyncAdd(4, 3, (sum) => {
        expect(sum).toBeA('number').toBe(7)
        done()
      })
    })

    it('shoould async square a number', (done) => {
      utils.asyncSquare(4, (squared) => {
        expect(squared).toBeA('number').toBe(16)
        done()
      })
    })
  })
})
