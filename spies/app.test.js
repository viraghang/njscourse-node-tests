/* global it,describe */

const expect = require('expect')
const rewire = require('rewire')

var app = rewire('./app')
// app.__set__
// app.__get__

describe('App', () => {
  var db = {
    saveUser: expect.createSpy()
  }
  app.__set__('db', db)

  it('should call the spy correctly', () => {
    let spy = expect.createSpy()
    spy('Andrew', 25)
    expect(spy).toHaveBeenCalled('Andrew', 25)
  })

  it('should call saveUser with user object', () => {
    let email = 'email@email.com'
    let password = '1234'

    app.handleSignup(email, password)
    expect(db.saveUser).toHaveBeenCalledWith({email, password})
  })
})
