/* global it,describe */

const request = require('supertest')
const expect = require('expect')

const app = require('./server').app

describe('Server', () => {
  describe('GET /', () => {
    it('should return "Hello World!" response', (done) => {
      request(app)
        .get('/')
        .expect(404)
        .expect(res => {
          expect(res.body).toInclude({
            error: 'Page not found'
          })
        })
        .end(done)
    })
  })

  describe('GET /users', () => {
    it('status code should be 200, I should be amongst users', (done) => {
      request(app)
        .get('/users')
        .expect(200)
        .expect(res => {
          expect(res.body).toInclude({
            name: 'Zoli',
            age: 33
          })
        })
        .end(done)
    })
  })
})
